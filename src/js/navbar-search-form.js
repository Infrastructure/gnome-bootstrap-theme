const searchbar = document.getElementById('.js-search');
const js_search_toggle = document.getElementById('.js-search-toggle')
if (js_search_toggle) {
    js_search_toggle.onclick = function () {
        searchbar.parent.closest('div').classList.toggle('is-visible');
        setTimeout(function () {
            searchbar.focus()
        }, 350)
    };
}