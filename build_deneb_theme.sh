#!/usr/bin/env sh

BOOTSTRAP_VERSION="$(grep bootstrap package.json | sed "s/\"bootstrap\":\s\"\^\([[:digit:]]\.[[:digit:]]\).*\"/\1/" | tr -d " ")"

OUTPUT_DIRECTORY="./dist.bootstrap-${BOOTSTRAP_VERSION}"
mkdir -p "${OUTPUT_DIRECTORY}"

# CSS
sassc -t expanded src/css/main.scss >"${OUTPUT_DIRECTORY}/deneb.${BOOTSTRAP_VERSION}.css"
sassc -t compressed src/css/main.scss >"${OUTPUT_DIRECTORY}/deneb.${BOOTSTRAP_VERSION}.min.css"

cp ./node_modules/bootstrap/dist/css/bootstrap.rtl.min.css "${OUTPUT_DIRECTORY}/deneb.rtl.${BOOTSTRAP_VERSION}.min.css"
cp ./node_modules/bootstrap/dist/css/bootstrap.rtl.min.css.map "${OUTPUT_DIRECTORY}/deneb.rtl.${BOOTSTRAP_VERSION}.min.css.map"
sed -i "s/bootstrap.rtl.min.css.map/deneb.rtl.${BOOTSTRAP_VERSION}.min.css.map/g" "${OUTPUT_DIRECTORY}/deneb.rtl.${BOOTSTRAP_VERSION}.min.css"

# JS
cat ./node_modules/bootstrap/dist/js/bootstrap.bundle.js src/js/navbar-search-form.js >"${OUTPUT_DIRECTORY}/deneb.${BOOTSTRAP_VERSION}.js"
cp ./node_modules/bootstrap/dist/js/bootstrap.bundle.js.map "${OUTPUT_DIRECTORY}/deneb.${BOOTSTRAP_VERSION}.js.map"
sed -i "s/bootstrap.bundle.js.map/deneb.${BOOTSTRAP_VERSION}.js.map/g" "${OUTPUT_DIRECTORY}/deneb.${BOOTSTRAP_VERSION}.js"
uglifyjs "${OUTPUT_DIRECTORY}/deneb.${BOOTSTRAP_VERSION}.js" -c -m -o "${OUTPUT_DIRECTORY}/deneb.${BOOTSTRAP_VERSION}.min.js"

# FontAwesome Webfonts
cp -r ./node_modules/fork-awesome/fonts "${OUTPUT_DIRECTORY}/fonts"

# Finalise
cp -r "${OUTPUT_DIRECTORY}/" "./dist/"
