# GNOME Deneb

[Bootstrap](https://getbootstrap.com/) theme for GNOME websites.

## Compile the Deneb theme

In order to build this theme, you need to install `npm` in order to retrieve the dependencies (`bootstrap` and `fontawesome`) used to build the CSS/JS/Fonts files. To compile SCSS stylesheets, it uses the `sassc` command line program and `uglifyjs` to compress the JS files.

On Fedora, packages are called `sassc`, `uglify-js` and `npm`, as in Alpine Linux (used in the CI process).

To retrieve the necessary dependencies, run: 
 ```bash
 npm install
 ```

This will download both Bootstrap and Font-Awesome SCSS files. Then, if all the other dependencies are installed on your system, you can run the `build_deneb_theme.sh` shell script. All the theme files will be stored in the `dist` directory.

This includes:

 * The full `bootstrap` css classes and features
 * A GNOME specific theme (with adapted colors and classes). An example is shown in the `index.html` file.
 * Icons provided by the `fontawesome` project. It is used to include some action icons for instance.

## Dependencies and Licence Issues

* [`bootstrap`](https://getbootstrap.com/docs/5.2) is released under the [**MIT LICENSE**](https://github.com/twbs/bootstrap/blob/main/LICENSE).
* [`fontawesome`](https://fontawesome.com) is released under a [**free license**](https://github.com/FortAwesome/Font-Awesome/blob/6.x/LICENSE.txt). We use the free package of this project.